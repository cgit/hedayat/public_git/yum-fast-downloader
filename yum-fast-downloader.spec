Summary:	A fast downloader plugin for yum
Name:		yum-fast-downloader
Version:	0.6.2
Release:	1
License:	GPLv2+
Group:		System Environment/Base
Source:		%{name}-%{version}.tar.bz2
BuildArch:	noarch
Requires:       aria2

%description
This plugin intends to speedup yum downloads by using a download manager
application (currently Aria 2) for downloading files instead of URLGrabber
which is used by yum.
Currently, it provides the following features:
* Using fastest mirrors for downloading
* Downloading repository metadata from several mirrors simultaneously
* Downloading multiple packages in parallel from several mirrors
* Downloading delta rpms

%prep
%setup -q

%install
install -d -m 755 $RPM_BUILD_ROOT/usr/lib/yum-plugins \
      $RPM_BUILD_ROOT/etc/yum/pluginconf.d/ $RPM_BUILD_ROOT/usr/libexec/
install -m 644 %{name}.py $RPM_BUILD_ROOT/usr/lib/yum-plugins/
install -m 644 %{name}.conf $RPM_BUILD_ROOT/etc/yum/pluginconf.d/
install -m 755 %{name}-finalize $RPM_BUILD_ROOT/usr/libexec/
install -m 755 urlgrabber-ext-down-yfd $RPM_BUILD_ROOT/usr/libexec/

%files
%config(noreplace) /etc/yum/pluginconf.d/%{name}.conf
%doc README COPYING
/usr/lib/yum-plugins/*
/usr/libexec/*

%changelog
* Sat Oct 11 2014 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.6.2-1
- Fix downloading from https links

* Wed Jul 16 2014 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.6.1-1
- Fix bug when dealing with OpenSUSE ubuid service repos (fix bug in metalink
  downloads)

* Sat Mar 29 2014 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.6.0-1
- Support recent versions of yum which integrate presto
- Integrate with urlgrabber and register as an external downloader for it.
  Now everything (except the initial repomd.xml files) is handled by YFD
- Additional argumens for aria2c can be specified in config now

* Fri Jan 25 2013 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.12-1
- Import URLGrabError

* Fri Jan 25 2013 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.11-0
- Import Errors

* Wed Jun 27 2012 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.10-1
- Update for the latest urlgrabber options

* Mon Jun 04 2012 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.9-2
- Bump release, since the previous release was using Fedora 14 code :(

* Tue May 29 2012 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.9-1
- Make sure that URL line is not too long for aria2

* Wed Oct 19 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.8-1
- Include repository name in output when downloading metadata files

* Sat Oct 08 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.7-1
- Fixed a bug in downloading single files which caused repeated downloading of
  metadata files

* Thu Oct 06 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.6-1
- Try to prevent re-downloading after some package downloads

* Wed Jul 13 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.5-1
- Make sure that the yfd file is renamed before proceeding

* Sun Jun 12 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.4-1
- Save intermediate download files with a different name, so that presto
  will not remove aria2c incompletely downloaded files

* Fri May 13 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.3-1
- Enable split download for aria2-1.11

* Thu May 12 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.2-1
- Fixed a small bug and add a new config option in 0.5.2

* Wed May 11 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5.1-1
- Renamed plugin to yum-fast-downloader so that it'll be run after presto
- Added README to the package as doc
- Added COPYING file

* Wed May 11 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5-2
- Added aria2 as a requirement

* Wed May 11 2011 Hedayat Vatankhah <hedayat.fwd+rpmchlog@gmail.com> - 0.5-1
- Initial version

