#! /usr/bin/python -tt
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
#
# Copyright Hedayat Vatankhah, 2011.
#
# Author: Hedayat Vatankhah <hedayat.fwd@gmail.com>
#
# * Used local and presto plugins and yum's code to learn more about yum 
# internals.
# 
#

import os
import yum
import subprocess
from yum.plugins import TYPE_CORE
from urlgrabber.grabber import _ExternalDownloader

requires_api_version = '2.6'
plugin_type = (TYPE_CORE,)

# variables
originalExtStart = _ExternalDownloader.start
downloader_app = 'aria2c'
global_cache_dir=''
downloader_common_args = {
    'aria2c' : ["aria2c", "--continue", "--check-certificate=false",
                 "--on-download-complete=/usr/libexec/yum-fast-downloader-finalize"]
}

# Hooks! 
def myExtDownloaderInit(self):
    self.popen = subprocess.Popen(
        ['/usr/libexec/urlgrabber-ext-down-yfd', global_cache_dir] +
            downloader_common_args[downloader_app],
        stdin = subprocess.PIPE,
        stdout = subprocess.PIPE,
    )
    self.stdin  = self.popen.stdin.fileno()
    self.stdout = self.popen.stdout.fileno()
    self.running = {}
    self.cnt = 0

def myExtDownloaderStart(self, opts):
    opts.urls = [m['mirror'] for m in opts.mirror_group[0].mirrors]
    originalExtStart(self, opts)

def postconfig_hook(conduit):
    global downloader_app
    global global_cache_dir
    global downloader_common_args
    downloader_app = conduit.confString('main', 'downloader', default='aria2c')
    global_cache_dir = conduit.getConf().cachedir
    max_concurrent_downloads = conduit.confString('aria2c',
             'max-concurrent-downloads', default='10')
    min_split_size = conduit.confString('aria2c',
             'min_split_size', default='1M')
    max_connection_per_server = conduit.confString('aria2c',
             'max_connection_per_server', default='5')
    aria2_additional_args = conduit.confString('aria2c', 'additional_args',
             default='')

    # append aria2c options
    downloader_common_args['aria2c'].append(
        "--server-stat-if={0}/aria2c_server_stats".format(global_cache_dir))
    downloader_common_args['aria2c'].append(
        "--server-stat-of={0}/aria2c_server_stats".format(global_cache_dir))
    downloader_common_args['aria2c'].append(
        "--max-concurrent-downloads={0}".format(max_concurrent_downloads))
    downloader_common_args['aria2c'].append(
        "--min-split-size={0}".format(min_split_size))
    downloader_common_args['aria2c'].append(
        "--max-connection-per-server={0}".format(max_connection_per_server))
    if aria2_additional_args:
        downloader_common_args['aria2c'] += aria2_additional_args.split(' ')

def prereposetup_hook(conduit):
    downloader_insmethod = type(_ExternalDownloader.__init__)
    _ExternalDownloader.__init__ = downloader_insmethod(myExtDownloaderInit, None, _ExternalDownloader)
    _ExternalDownloader._options+=('urls', 'relative_url')

    start_method = type(_ExternalDownloader.start)
    _ExternalDownloader.start = start_method(myExtDownloaderStart, None, _ExternalDownloader)
    _ExternalDownloader.repos = conduit.getRepos().listEnabled()
#    repos = conduit.getRepos()
#    for repo in repos.listEnabled():
#       print "name: %s" % repo.name
#       print "URLS: %s" % repo.urls
#       repo._retrieveMD = testRetrieveMD
